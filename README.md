# OpenML dataset: Swedish-central-bank-interest-rate-and-inflation

https://www.openml.org/d/43528

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This a blend dataset that contains historic Swedish interest rates from 1908-2001 Source/Klla: Sveriges riksbank and Swedish inflation rate 1908-2001 fetched from Sweden's statistic central bureau SCB.
Content:
Blend of Swedish historic central bank interest rate diskkonto and Swedish SCB Consument price index
Acknowledgements / Original data sets:
Swedish central bank interest rate diskkonto
http://www.riksbank.se/sv/Rantor-och-valutakurser/Sok-rantor-och-valutakurser/
Consumer price index
http://www.scb.se/sv_/Hitta-statistik/Statistik-efter-amne/Priser-och-konsumtion/Konsumentprisindex/Konsumentprisindex-KPI/33772/33779/Konsumentprisindex-KPI/33831/
Data set cover images: 
Wikipedia https://sv.wikipedia.org/wiki/Enkronan/media/File:1_Krona_1927,_1.jpg
https://en.wikipedia.org/wiki/Flag_of_Sweden/media/File:Flag_of_Sweden.svg
Inspiration: 
Question: How does central bank interest rate effect inflation? What are the interest rate inflation rate delays?
Verify ROC R2 inflation/interest rate causation.
Content:
Interestrate and inflation Sweden 1908-2001.csv
Columns

Period    Year 
Central bank interest rate diskonto average    Percent
Inflation    Percent   
Price level Integer

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43528) of an [OpenML dataset](https://www.openml.org/d/43528). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43528/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43528/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43528/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

